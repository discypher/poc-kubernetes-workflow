require "sinatra"
require "faraday"
require "json"
require "logger"

$stdout.sync = true

class ExampleApp < Sinatra::Base

  set :port, ENV['SERVICE_PORT'] || "8080"

  configure :development do
    logger = Logger.new(STDOUT)
    logger.level = Logger::DEBUG
    set :raise_errors, true
    set :logger, logger

    set :jwt_path, nil
    set :vault_url, ENV["VAULT_ADDR"] || "http://localhost:8200"
  end

  configure :production do
    logger = Logger.new(STDOUT)
    logger.level = Logger::INFO
    set :jwt_path, ENV["JWT_PATH"]
    set :vault_url, ENV["VAULT_ADDR"] || "http://vault:8200"
  end

  # GET "/"
  get "/" do
    logger.info "Received Request - Port forwarding is working."
    secrets = File.read "/vault/secrets/database-config.txt"
    secrets.to_s
  end
end
