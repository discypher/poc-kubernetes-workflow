# Proof of Concept - Kubernetes Workflow
## Tools
- Helm
- Skaffold
- Vault/Consul
- Kustomize (maybe)
- Minikube
- Ruby

## References
* [Vault Installation to Minikube via Helm \| Vault - HashiCorp Learn](https://learn.hashicorp.com/vault/kubernetes/minikube)
* [skaffold/examples/helm-deployment at master · GoogleContainerTools/skaffold · GitHub](https://github.com/GoogleContainerTools/skaffold/tree/master/examples/helm-deployment)
* [Helm \| Skaffold](https://skaffold.dev/docs/pipeline-stages/deployers/helm/)
* [Injecting Secrets into Kubernetes Pods via Vault Helm Sidecar \| Vault - HashiCorp Learn](https://learn.hashicorp.com/vault/kubernetes/sidecar)


## Install
I am using `asdf` to install the majority of these tools. If you do not use `asdf` you can  
find installation instructions at the website for each tool. I am also setting the tool  
version globally. You can substitute `local` for `global` in the asdf commands.  

Vault and Consul will be installed via Helm into the Minikube Cluster  

### Kubectl/Kustomize (maybe)
This will be set up using `asdf`  
```
asdf plugin add kubectl
asdf install kubectl 1.18.2
asdf global kubectl 1.18.2
```

### Minikube
This will be set up using `asdf`  
```
asdf plugin add minikube
asdf install minikube 1.10.1
asdf global minikube 1.10.1
```

### Helm
This will be set up using `asdf`  
```
asdf plugin add helm
asdf install helm 3.2.1
asdf global helm 3.2.1
```

### Skaffold
This will be set up using `asdf`  
```
asdf plugin add skaffold
asdf install skaffold 1.10.1
asdf global skaffold 1.10.1
```

### Ruby
This gives you the same Ruby as the Docker Base image if you want that  
```
asdf plugin add ruby
asdf install ruby 2.6.3
asdf global ruby 2.6.3
```

### Vault/Consul
This will be set up using Helm. You will need to have minikube Running.  
```
minikube start
```

#### Consul
First create the consul values file  
`helm-consul-values.yml`
```
global:
  datacenter: vault-kubernetes-guide

client:
  enabled: true

server:
  replicas: 1
  bootstrapExpect: 1
  disruptionBudget:
    maxUnavailable: 0
```
Now we'll use Helm to install Consul  
```
helm install consul \
    --values helm-consul-values.yml \
    https://github.com/hashicorp/consul-helm/archive/v0.21.0.tar.gz
```
You can check that Consul in installed and running  
```
kubectl get po

NAME                     READY   STATUS    RESTARTS   AGE
consul-consul-2qcrz      1/1     Running   0          3m20s
consul-consul-server-0   1/1     Running   0          3m20s
```
#### Vault
Create the vault values file  
`helm-vault-values.yml`
```
server:
  affinity: ""
  ha:
    enabled: true
```
Now we'll use Helm to install Vault  
```
helm install vault \
    --values helm-vault-values.yml \
    https://github.com/hashicorp/vault-helm/archive/v0.5.0.tar.gz
```
You can check that Consul and Vault are installed and running  
Unseal your vault  
```
kubectl exec vault-0 -- vault operator init -key-shares=1 -key-threshold=1 -format=json > cluster-keys.json
VAULT_UNSEAL_KEY=$(cat cluster-keys.json | jq -r ".unseal_keys_b64[]")
kubectl exec vault-0 -- vault operator unseal $VAULT_UNSEAL_KEY
kubectl exec vault-1 -- vault operator unseal $VAULT_UNSEAL_KEY
kubectl exec vault-2 -- vault operator unseal $VAULT_UNSEAL_KEY
```

```
kubectl get po

NAME                                    READY   STATUS    RESTARTS   AGE
consul-consul-2qcrz                     1/1     Running   0          3m55s
consul-consul-server-0                  1/1     Running   0          3m55s
vault-0                                 1/1     Running   0          3m5s
vault-1                                 1/1     Running   0          3m4s
vault-2                                 1/1     Running   0          3m3s
vault-agent-injector-7cf6975f99-s5ggq   1/1     Running   0          3m5s

```
Get your root token from Vault  
```
cat cluster-keys.json | jq -r ".root_token"
s.3noDT67A9rWvOAhnZY0vMRGk
```
Now get a shell on a vault pod and login  
```
kubectl exec -it vault-0 -- /bin/sh
/ $ vault login s.3noDT67A9rWvOAhnZY0vMRGk
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                  Value
---                  -----
token                s.3noDT67A9rWvOAhnZY0vMRGk
token_accessor       5ziXEIKuaer08bWWpjNDWxZd
token_duration       ∞
token_renewable      false
token_policies       ["root"]
identity_policies    []
policies             ["root"]
```
Enable kv-v2 secrets  
```
/ $ vault secrets enable -path=secret kv-v2
Success! Enabled the kv-v2 secrets engine at: secret/
```
Create a secret for our web app  
```
vault kv put secret/webapp/config username="static-user" password="static-password"
Key              Value
---              -----
created_time     2020-05-24T22:54:18.405286333Z
deletion_time    n/a
destroyed        false
version          1
```
Enable Kubernetes Auth  
```
vault auth enable kubernetes
Success! Enabled kubernetes auth method at: kubernetes/
```
Configure the kubernetes auth method  
```
vault write auth/kubernetes/config \
    token_reviewer_jwt="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" \
    kubernetes_host="https://$KUBERNETES_PORT_443_TCP_ADDR:443" \
    kubernetes_ca_cert=@/var/run/secrets/kubernetes.io/serviceaccount/ca.crt
Success! Data written to: auth/kubernetes/config
```
Configure a policy for our webapp secret  
```
vault policy write webapp - <<EOF
path "secret/data/webapp/config" {
  capabilities = ["read"]
}
EOF
Success! Uploaded policy: webapp
```
Create an auth role that our service account can use, with the above policy  
```
vault write auth/kubernetes/role/webapp \
    bound_service_account_names=vault \
    bound_service_account_namespaces=default \
    policies=webapp \
    ttl=24h
Success! Data written to: auth/kubernetes/role/webapp
```
### Helm
Now we can create a helm chart within the project  
```
helm create poc-kubernetes-app-chart
Creating poc-kubernetes-app-chart
```
We need to update a few fields in `poc-kubernetes-workflow/values.yml`  
Change  
```
image:
  repository: nginx
```
To
```
image:
  repository: poc-kubernetes-workflow
```
And update `Chart.yml`  
```
apiVersion: 2
name: poc-kubernetes-app-chart
description: A helm chart to install a POC Kubernetes Workflow Application
...
appVersion: 0.0.1
```

Now we'll want to build the docker image using Minikube's docker daemon  
Note that this has to be done in every terminal window you want to build  
the image in.  
```
eval $(minikube docker-env)
```
And build the image (tag is based on appVersion in Chart.yml)  
```
docker build -t poc-kubernetes-workflow:0.0.1
```
Package up the Helm Chart  
```
helm package poc-kubernetes-app-chart
Successfully packaged chart and saved it to: /home/shad/workspace/playground/kubernetes-workflow/poc-kubernetes-app-chart-0.1.0.tgz
```
And install it  
```
helm install my-poc-app poc-kubernetes-app-chart-0.1.0.tgz
NAME: my-poc-app
LAST DEPLOYED: Sun May 24 18:50:45 2020
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
1. Get the application URL by running these commands:
  export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=poc-kubernetes-app-chart,app.kubernetes.io/instance=my-poc-app" -o jsonpath="{.items[0].metadata.name}")
  echo "Visit http://127.0.0.1:8080 to use your application"
  kubectl --namespace default port-forward $POD_NAME 8080:80

```

You now have your application in Helm and deployed to your Minikube Cluster.  

### Skaffold
We'll need a `skaffold.yml` file for Skaffold to run. Here we'll be using the  
`helm` deployer.  

```
# skaffold.yml
build:
  artifacts:
    - image: poc-kubernetes-workflow
deploy:
  helm:
    releases:
      - name: poc-kubernetes-app-chart
        chartPath: poc-kubernetes-app-chart
        artifactOverrides:
          image: poc-kubernetes-workflow
        valuesFile:
          - values.yaml

```

We'll need to adjust our Helm Chart to work with Skaffold. This is due to the way  
Skaffold is continuously rebuilding and tagging images.  
```
# poc-kubernetes-app-chart/values.yaml
...
imageConfig:
  pullPolicy: IfNotPresent
...
```
And
```
# poc-kubernetes-app-chart/templates/deployment.yaml
...
image: "{{ .Values.image }}" 
imagePullPolicy: {{ .Values.imageConfig.pullPolicy }}
...
```

Now run Skaffold  
```
skaffold dev
```
And make a change to `app.rb`  
```
...
get '/' do
  "Hello Skaffold!"
end
```
When you save that change, Skaffold will detect it and automatically build, tag, and  
deploy the Helm chart containing your changes.  

## Vault
To hook up Vault to our application we'll need to update the service account for the app  
```
# poc-kubernetes-app-chart/values.yaml
...
serviceAccount:
  create: false # Change this to false
  annotations: {}
  name: "vault" # Change this to "vault"
...
```

Skaffold should automatically pick up that change and update your Release.  

We now need to add the EnvVars we'll require to talk to Vault  
```
env:
  - name: VAULT_ADDR
    value: "http://vault:8200"
  - name: JWT_PATH
    value: "/var/run/secrets/kubernetes.io/serviceaccount/token"
  - name: SERVICE_PORT
    value: "80"
```

In order to read from Vault I've copied the application written by Lynn Frank  
at Hashicorp in their Minikube/Vault demo, as linked at the top of this document.  
This does not use in injection annotations and instead uses the vault service  
account jwt to communicate with Vault to read the secrets.  

## Vault Injection
Log back in to the `vault-0` pod.  
Create a new kv-v2 Secret store
```
/ $ vault secrets enable -path=internal kv-v2
Success! Enabled the kv-v2 secrets engine at: internal/
```
Create a new Secret in Vault for some app.  
```
vault kv put internal/database/config username="db-readonly-username" password="db-secret-password"
Key              Value
---              -----
created_time     2020-03-25T19:03:57.127711644Z
deletion_time    n/a
destroyed        false
version          1
```
We want to use a different service account this time.  
We'll need a new Vault Policy and Role.   
```
/ $ vault policy write internal-app - <<EOF
path "internal/data/database/config" {
  capabilities = ["read"]
}
EOF
Success! Uploaded policy: internal-app
/ $ vault write auth/kubernetes/role/internal-app \
        bound_service_account_names=internal-app \
        bound_service_account_namespaces=default \
        policies=internal-app \
        ttl=24h
Success! Data written to: auth/kubernetes/role/internal-app
```
We're going to let Helm create the Service Account for us in Kubernetes  
```
#poc-kubernetes-app-chart/values.yaml
serviceAccount:
  create: true
  annotations: {}
  name: "internal-app"
```

We'll also need to add some annotations to the Pods to tell the vault agent that   
it should inject secrets, which role to use, and what to call the file.  
```
podAnnotations: {
  vault.hashicorp.com/agent-inject: "true"
  vault.hashicorp.com/role: "internal-app"
  vault.hashicorp.com/agent-inject-secret-database-config.txt: "internal/data/database/config"
}

```
We'll be changing the application again to read secrets from the file created and  
print out that data.  
```
  get "/" do
    logger.info "Received Request - Port forwarding is working."
    secrets = File.read "/vault/secrets/database-config.txt"
    secrets.to_s
  end
```
