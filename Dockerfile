FROM ruby:2.6-alpine3.11

WORKDIR /app

ADD Gemfile Gemfile.lock ./

RUN apk update && apk upgrade && \
    gem install bundler && \
    bundle install

ADD . ./
EXPOSE 80

ENTRYPOINT ["rackup", "--port", "80", "--env", "production", "-o", "0.0.0.0"]