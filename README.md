# Kubernetes Workflow Proof of Concept
## What we're solving
We'd like to have a workflow where we can develop applications which will  
be deployed to Kubernetes. The application should deploy automatically  
to a local cluster. We would like to use Helm to manage dependencies and  
remove some of the boilerplate code for Kubernetes resources. Secrets  
should be resolved through Hashicorp Vault. We also need the option to  
override configuration of resources at deploy time.

## Tools used in this project
- `asdf-vm`
- `ruby` and `bundler`
- `Hashicorp Vault`
- `Hashicorp Consul`
- `Minikube`
- `Helm v3`
- `Skaffold`
- `Kustomize` can be used at deploy time to override configuration

## References I used for this project
[Vault Installation to Minikube via Helm \| Vault - HashiCorp Learn](https://learn.hashicorp.com/vault/kubernetes/minikube)  
[skaffold/examples/helm-deployment at master · GoogleContainerTools/skaffold · GitHub](https://github.com/GoogleContainerTools/skaffold/tree/master/examples/helm-deployment)  
[Helm \| Skaffold](https://skaffold.dev/docs/pipeline-stages/deployers/helm/)  
[Injecting Secrets into Kubernetes Pods via Vault Helm Sidecar \| Vault - HashiCorp Learn](https://learn.hashicorp.com/vault/kubernetes/sidecar)  

## Getting Started
First we need to get some tools set up. We'll be using `asdf-vm` to install  
the tools for this project. If you do not use `asdf-vm** you can find  
detailed instructions for each tool on their respective websites.

** Run the following commands within this project director **

### Install Kubectl/Kustomize
```
$ asdf plugin add kubectl
$ asdf install kubectl 1.18.2
$ asdf local kubectl 1.18.2
```

### Install Minikube
```
$ asdf plugin add minikube
$ asdf install minikube 1.10.1
$ asdf local minikube 1.10.1
```

### Install Helm
```
$ asdf plugin add helm
$ asdf install helm 3.2.1
$ asdf local helm 3.2.1
```

### Install Skaffold
```
$ asdf plugin add skaffold
$ asdf install skaffold 1.10.1
$ asdf local skaffold 1.10.1
```

### Install Ruby
```
$ asdf plugin add ruby
$ asdf install ruby 2.6.3
$ asdf local ruby 2.6.3
```

### Install Virtualbox
If you don't already have virtualbox installed, you can either install according  
to the instructions at https://www.virtualbox.org/wiki/Downloads

Or, if you're on a Mac using Homebrew
```
$ brew install virtualbox virtualbox-extension-pack
```

### Start Minikube
Minikube will need to be running if it is not already.
```
$ minikube start --driver=virtualbox -p poc-kube-workflow

```

Now set your minikube Profile
```
$ minikube profile poc-kube-workflow
✅  minikube profile was successfully set to poc
```

And make sure it's up and running
```
$ minikube status
poc-kube-workflow
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

### Make sure you have the right Kubernetes Context loaded
```
$ kubectl config use-context poc-kube-workflow
```

### Hashicorp Consul Installation
We're going to install `Hashicorp Consul` using `Helm`. It will be used at the backend  
for `Hashicorp Vault`.
#### Install Consul
We will override some of the default `Consul Helm Chart` settings. To do this, create  
a values file (or use the one in the `setup` directory).
```
# helm-consul-values.yml
global:
  datacenter: vault-kubernetes-guide

client:
  enabled: true

server:
  replicas: 1
  bootstrapExpect: 1
  disruptionBudget:
    maxUnavailable: 0
 
```

We'll use that file to install `Consul`  
```
$ helm install consul \
    --values setup/helm-consul-values.yml \
    https://github.com/hashicorp/consul-helm/archive/v0.21.0.tar.gz
```

You can check that Consul is up and running in `Minikube`  
```
$ kubectl get po

NAME                     READY   STATUS    RESTARTS   AGE
consul-consul-2qcrz      1/1     Running   0          3m20s
consul-consul-server-0   1/1     Running   0          3m20s
```

### Hashicorp Vault Installation
We're going to install `Hashicorp Vault` using `Helm`.  

#### Install and Unseal Vault
We will override some of the default `Vault Helm Chart` settings. To do this, create  
a values file (or use the one in the `setup` directory).
```
server:
  affinity: ""
  ha:
    enabled: true
```

We'll use that file to install `Vault`  
```
$ helm install vault \
    --values setup/helm-vault-values.yml \
    https://github.com/hashicorp/vault-helm/archive/v0.5.0.tar.gz
```

If you check the status of your vault pods now they will be unhealthy
```
$ kubectl get pods
NAME                                    READY   STATUS    RESTARTS   AGE
consul-consul-ln2r6                     1/1     Running   0          14m
consul-consul-server-0                  1/1     Running   0          14m
vault-0                                 0/1     Running   0          9m36s
vault-1                                 0/1     Running   0          9m35s
vault-2                                 0/1     Running   0          9m34s
vault-agent-injector-7cf6975f99-clsjv   1/1     Running   0          9m37s
```

You have to unseal them before the readiness probe will pass.

We'll need to initialize and unseal the vault. You can do this from one of the  
`Vault` Pods in the Cluster. We'll initialize the vault, store the keys locally  
in a file called `cluster-keys.json`, stick that in an environment variable  
and then use it to unseal each Pod.
```
$ kubectl exec vault-0 -- vault operator init -key-shares=1 -key-threshold=1 -format=json > cluster-keys.json
$ VAULT_UNSEAL_KEY=$(cat cluster-keys.json | jq -r ".unseal_keys_b64[]")
$ kubectl exec vault-0 -- vault operator unseal $VAULT_UNSEAL_KEY
...
$ kubectl exec vault-1 -- vault operator unseal $VAULT_UNSEAL_KEY
...
$ kubectl exec vault-2 -- vault operator unseal $VAULT_UNSEAL_KEY
...
```

You can check that Vault is up and running in `Minikube`  
``**
$ kubectl get po

NAME                                    READY   STATUS    RESTARTS   AGE
consul-consul-2qcrz                     1/1     Running   0          3m55s
consul-consul-server-0                  1/1     Running   0          3m55s
vault-0                                 1/1     Running   0          3m5s
vault-1                                 1/1     Running   0          3m4s
vault-2                                 1/1     Running   0          3m3s
vault-agent-injector-7cf6975f99-s5ggq   1/1     Running   0          3m5s
`

### Configure Vault
Now that Vault has been installed, initialized, and unsealed we can do some  
configuration to support the sample app and create some secrets.

#### Login with root token
Get your root token from Vault  
```
$ cat cluster-keys.json | jq -r ".root_token"
s.3noDT67A9rWvOAhnZY0vMRGk
```

---
** Note: Make sure you are executing these commands inside of a Pod! **
---


Get a shell on a `vault` pod and login  
```
$ kubectl exec -it vault-0 -- /bin/sh
/ $ vault login s.3noDT67A9rWvOAhnZY0vMRGk
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                  Value
---                  -----
token                s.3noDT67A9rWvOAhnZY0vMRGk
token_accessor       5ziXEIKuaer08bWWpjNDWxZd
token_duration       ∞
token_renewable      false
token_policies       ["root"]
identity_policies    []
policies             ["root"]
```

#### Enable kv-v2 secrets
We need a secret store for our app. We're going to use the `kv-v2` Secrets  
Engine for this. From the pod we're logged in on:
```
/ $ vault secrets enable -path=internal kv-v2
Success! Enabled the kv-v2 secrets engine at: internal/
```

#### Create a Secret
From the pod we're logged in on:
```
vault kv put internal/database/config username="db-readonly-username" password="db-secret-password"
Key              Value
---              -----
created_time     2020-03-25T19:03:57.127711644Z
deletion_time    n/a
destroyed        false
version          1
```

#### Create a Vault Policy
From the pod we're logged in on:
```
/ $ vault policy write internal-app - <<EOF
path "internal/data/database/config" {
  capabilities = ["read"]
}
EOF
Success! Uploaded policy: internal-app
```

#### Enable the Kubernetes Auth Backend
We need to let Kubernetes Service Accounts Authenticate to Vault
```
$ vault auth enable kubernetes
Success! Enabled kubernetes auth method at: kubernetes/
```

Configure the Kubernetes Auth method:
```
/ $ vault write auth/kubernetes/config \
        token_reviewer_jwt="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" \
        kubernetes_host="https://$KUBERNETES_PORT_443_TCP_ADDR:443" \
        kubernetes_ca_cert=@/var/run/secrets/kubernetes.io/serviceaccount/ca.crt
Success! Data written to: auth/kubernetes/config
```

#### Create a Vault Role
From the pod we're logged in on:
```
/ $ vault write auth/kubernetes/role/internal-app \
        bound_service_account_names=internal-app \
        bound_service_account_namespaces=default \
        policies=internal-app \
        ttl=24h
Success! Data written to: auth/kubernetes/role/internal-app
```

#### Exit the Pod
We no longer need to be logged in on the `Vault` pod
```
/ $ exit
```
### What we just did
We now have all of the tools we need to do development work on this project. The  
next step is to use the existing application and `Helm Chart` to make some  
changes and deploy our application to `Minikube`. There are more notes on what  
was done initially to generate the `Helm` Chart and configure the application  
to use `Vault` to inject secrets in the `setup/notes.md` file found in this  
project.

## Development With Skaffold
First we need to tell Skaffold to use the local cluster.
This is a one-time step unless you want to switch to another cluster.
```
$ skaffold config set --global local-cluster true
```

We need to tell it to use minikube's Docker Daemon
```
eval $(minikube docker-env)
```

Now, in your favorite Terminal run:
```
$ skaffold dev
```

You should see output like so:
```
Listing files to watch...
 - poc-kubernetes-workflow
Generating tags...
 - poc-kubernetes-workflow -> poc-kubernetes-workflow:0297568-dirty
Checking cache...
 - poc-kubernetes-workflow: Not found. Building
Found [minikube] context, using local docker daemon.
Building [poc-kubernetes-workflow]...
Sending build context to Docker daemon  191.5kB
Step 1/7 : FROM ruby:2.6-alpine3.11 2
 ---> 174df7e95528
Step 2/7 : WORKDIR /app
 ---> Using cache
 ---> 1645502bbb98
Step 3/7 : ADD Gemfile Gemfile.lock ./
 ---> Using cache
 ---> b516836f16fc
Step 4/7 : RUN apk update && apk upgrade &&     gem install bundler &&     bundle install
 ---> Using cache
 ---> 0e4b975296a4
Step 5/7 : ADD . ./
 ---> 7d68b0a69189
Step 6/7 : EXPOSE 80
 ---> Running in b84e07027dfd
 ---> 963fb452e2a0
Step 7/7 : ENTRYPOINT ["rackup", "--port", "80", "--env", "production", "-o", "0.0.0.0"]
 ---> Running in 13b781190032
 ---> cce186ebbfdb
Successfully built cce186ebbfdb
Successfully tagged poc-kubernetes-workflow:0297568-dirty
Tags used in deployment:
 - poc-kubernetes-workflow -> poc-kubernetes-workflow:cce186ebbfdbb7aa67198533f6824134cbf5268eb24b22c120036d0b895255f0
Starting deploy...
Helm release poc-kubernetes-app-chart not installed. Installing...
NAME: poc-kubernetes-app-chart
LAST DEPLOYED: Mon May 25 19:18:49 2020
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
1. Get the application URL by running these commands:
  export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=poc-kubernetes-app-chart,app.kubernetes.io/instance=poc-kubernetes-app-chart" -o jsonpath="{.items[0].metadata.name}")
  echo "Visit http://127.0.0.1:8080 to use your application"
  kubectl --namespace default port-forward $POD_NAME 8080:80
WARN[0002] error adding label to runtime object: patching resource default/"poc-kubernetes-app-chart-test-connection": pods "poc-kubernetes-app-chart-test-connection" not found 
Waiting for deployments to stabilize...
 - deployment/poc-kubernetes-app-chart: waiting for rollout to finish: 0 of 1 updated replicas are available...
 - deployment/poc-kubernetes-app-chart is ready.
Deployments stabilized in 2.523503126s
Press Ctrl+C to exit
Watching for changes...
...
```

You'll notice there are a some lines of log output from the Vault Agent injecting  
Secrets into our Pods as they come up.

Now make some change to `lib/service.rb` and save the file. Notice that your app  
has automatically been rebuilt and redeployed using `Helm`.
```
Generating tags...
 - poc-kubernetes-workflow -> poc-kubernetes-workflow:0297568-dirty
Checking cache...
 - poc-kubernetes-workflow: Not found. Building
Found [minikube] context, using local docker daemon.
Building [poc-kubernetes-workflow]...
Sending build context to Docker daemon  191.5kB
Step 1/7 : FROM ruby:2.6-alpine3.11
 ---> 174df7e95528
Step 2/7 : WORKDIR /app
 ---> Using cache
 ---> 1645502bbb98
Step 3/7 : ADD Gemfile Gemfile.lock ./u
 ---> Using cache
 ---> b516836f16fc
Step 4/7 : RUN apk update && apk upgrade &&     gem install bundler &&     bundle install
 ---> Using cache
 ---> 0e4b975296a4
Step 5/7 : ADD . ./
 ---> 68766409af3b
Step 6/7 : EXPOSE 80
 ---> Running in d4ab656d66bd
 ---> f00342efbbf1
Step 7/7 : ENTRYPOINT ["rackup", "--port", "80", "--env", "production", "-o", "0.0.0.0"]
 ---> Running in 1ae3981f8afc
 ---> 1f5672466969
Successfully built 1f5672466969
Successfully tagged poc-kubernetes-workflow:0297568-dirty
Tags used in deployment:
 - poc-kubernetes-workflow -> poc-kubernetes-workflow:1f5672466969cb9a68809c1898b9662403e84f9a90062bf27a85adf6fdccf0be
Starting deploy...
Release "poc-kubernetes-app-chart" has been upgraded. Happy Helming!
NAME: poc-kubernetes-app-chart
LAST DEPLOYED: Mon May 25 19:22:21 2020
NAMESPACE: default
STATUS: deployed
REVISION: 2
NOTES:
1. Get the application URL by running these commands:
  export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=poc-kubernetes-app-chart,app.kubernetes.io/instance=poc-kubernetes-app-chart" -o jsonpath="{.items[0].metadata.name}")
  echo "Visit http://127.0.0.1:8080 to use your application"
  kubectl --namespace default port-forward $POD_NAME 8080:80
WARN[0214] error adding label to runtime object: patching resource default/"poc-kubernetes-app-chart-test-connection": pods "poc-kubernetes-app-chart-test-connection" not found 
Waiting for deployments to stabilize...
 - deployment/poc-kubernetes-app-chart: waiting for rollout to finish: 1 old replicas are pending termination...
 - deployment/poc-kubernetes-app-chart is ready.
Deployments stabilized in 10.451151795s
Watching for changes...
```

You can check the response from the app by following the NOTES section  
in the output from `Skaffold`.
```
$ export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=poc-kubernetes-app-chart,app.kubernetes.io/instance=poc-kubernetes-app-chart" -o jsonpath="{.items[0].metadata.name}")
$ kubectl --namespace default port-forward $POD_NAME 8080:80
```

And in another terminal window/tab:
```
$ curl localhost:8080
data: map[password:db-secret-password username:db-readonly-username]
metadata: map[created_time:2020-05-25T15:21:35.479888246Z deletion_time: destroyed:false version:1]

```

## A note on Kustomize
When this application is ready to deploy, should we have a need to change any  
of the Kubernetes Resource YAML files contained in the `Helm Chart`, and we  
have reason not to make that change within in the `Helm Chart` itself, we can  
provide a Post Renderer flag to `Helm`, as outlined [here](https://helm.sh/docs/topics/advanced/#usage).

## Conclusion
This project provides a proof of concept for a Kubernetes Development Workflow  
using industry standard, open-source tools.
